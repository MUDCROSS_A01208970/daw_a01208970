--Nombre del material, cantidad de veces entregados y total del costo de dichas entregas por material de todos los proyectos. 

Create view totales (ID,Nombre, Proyecto,costo, Total,CostoTotal)
as select  m.Clave,m.Descripcion,p.Denominacion ,m.Costo,Count(Cantidad) as 'Total de Entregas',SUM(e.Cantidad* (m.costo-((m.PorcentajeImpuesto/100)*m.Costo))) as 'Costo Total'
from  entregan e, Proyectos p,Materiales m
where e.Numero=p.Numero  and e.Clave=m.Clave 
 
 group by  m.Clave,m.Descripcion,p.Denominacion ,m.Costo
 
 select * from totales
