-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-03-2019 a las 02:27:30
-- Versión del servidor: 5.5.57-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `Servicios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Servicios`
--

CREATE TABLE IF NOT EXISTS `Servicios` (
  `IDServicios` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) NOT NULL,
  `Descripcion` text NOT NULL,
  `IDDepartamento` int(11) NOT NULL,
  PRIMARY KEY (`IDServicios`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `Servicios`
--

INSERT INTO `Servicios` (`IDServicios`, `Nombre`, `Descripcion`, `IDDepartamento`) VALUES
(1, 'Inyeccion', 'Proporciono inyeccion en vena', 1),
(2, 'Consulta', 'consulta de diabetes', 3),
(3, 'Aplicacion de insulina', 'Inyeccion a paciente con diabetes', 2),
(4, 'inyeccion', 'inyeccion a paciente', 2),
(6, 'inyeccion', 'inyeccion a paciente', 3),
(10, 'Consulta', 'inyeccion a paciente en brazo', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
