--Materiales
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'crearMaterial' AND type = 'P')
DROP PROCEDURE crearMaterial 
GO
CREATE PROCEDURE crearMaterial
@uclave NUMERIC(5,0),
@udescripcion VARCHAR(50),
@ucosto NUMERIC(8,2),
@uimpuesto NUMERIC(6,2)
AS
	INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
GO
--ejecutar el procedure
EXECUTE crearMaterial 5000,'Material Acme',250,15;
-- probarlo
select * from Materiales
--modificar
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'modificarMaterial' AND type = 'P')
DROP PROCEDURE modificarMaterial 
GO
CREATE PROCEDURE modificarMaterial
@uclave NUMERIC(5,0),
@udescripcion VARCHAR(50),
@ucosto NUMERIC(8,2),
@uimpuesto NUMERIC(6,2)
AS
	UPDATE Materiales SET Clave=@uclave,Descripcion=@udescripcion,Costo=@ucosto,PorcentajeImpuesto=@uimpuesto WHERE clave=@uclave
GO
--eliminar
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'eliminarMaterial' AND type = 'P')
DROP PROCEDURE eliminarMaterial 
GO
CREATE PROCEDURE eliminarMaterial
@uclave NUMERIC(5,0)
AS
	DELETE FROM Materiales WHERE Clave=@uclave
GO
-- Proyecto 
--crearProyecto
sp_help Proyectos
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'crearProyecto' AND type = 'P')
DROP PROCEDURE crearProyecto 
GO
CREATE PROCEDURE crearProyecto
@uNumero NUMERIC(5,0),
@uDenominacion VARCHAR(50)
AS
	INSERT INTO Proyectos VALUES(@uNumero, @uDenominacion)
GO
--modificar
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'modificarProyecto' AND type = 'P')
DROP PROCEDURE modificarProyecto 
GO
CREATE PROCEDURE modificarProyecto
@uNumero NUMERIC(5,0),
@uDenominacion VARCHAR(50)
AS
	UPDATE Proyectos SET Numero=@uNumero,Denominacion=@uDenominacion WHERE Numero=@uNumero
GO
--eliminar
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'eliminarProyecto' AND type = 'P')
DROP PROCEDURE eliminarProyecto 
GO
CREATE PROCEDURE eliminarProyecto
@uNumero NUMERIC(5,0)
AS
	DELETE FROM Proyectos WHERE Numero=@uNumero
GO
-- Proveedores 
--crearProveedores
sp_help Proveedores
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'crearProveedores' AND type = 'P')
DROP PROCEDURE crearProveedores 
GO
CREATE PROCEDURE crearProveedores
@uRFC CHAR(13),
@uRazonSocial VARCHAR(50)
AS
	INSERT INTO Proveedores VALUES(@uRFC, @uRazonSocial)
GO
--modificarProveedores
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'modificarProveedores' AND type = 'P')
DROP PROCEDURE modificarProveedores 
GO
CREATE PROCEDURE modificarProveedores
@uRFC CHAR(13),
@uRazonSocial VARCHAR(50)
AS
	UPDATE Proveedores SET RFC=@uRFC,RazonSocial=@uRazonSocial WHERE RFC=@uRFC
GO
--eliminarProveedores
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'eliminarProveedores' AND type = 'P')
DROP PROCEDURE eliminarProveedores 
GO
CREATE PROCEDURE eliminarProveedores
@uRFC CHAR(13)
AS
	DELETE FROM Proveedores WHERE  RFC=@uRFC
GO

--ENTREGAN

--crearEntrega
sp_help Entregan
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'crearEntrega' AND type = 'P')
DROP PROCEDURE crearEntrega 
GO
CREATE PROCEDURE crearEntrega
@uClave NUMERIC(5,0),
@uRFC CHAR(13),
@uNumero NUMERIC(5,0),
@uFecha datetime,
@uCantidad NUMERIC(8,2)
AS
	INSERT INTO Entregan VALUES(@uClave, @uRFC,@uNumero,@uFecha,@uCantidad)
GO
--modificarEntrega
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'modificarEntrega' AND type = 'P')
DROP PROCEDURE modificarEntrega 
GO
CREATE PROCEDURE modificarEntrega
@uClave NUMERIC(5,0),
@uRFC CHAR(13),
@uNumero NUMERIC(5,0),
@uFecha datetime,
@uCantidad NUMERIC(8,2)
AS
	UPDATE Entregan SET Clave=@uClave,RFC=@uRFC,Numero=@uNumero,Fecha=@uFecha,Cantidad=@uCantidad WHERE Clave=@uClave AND RFC=@uRFC AND Numero=@uNumero AND Fecha=@uFecha AND Cantidad=@uCantidad
GO
--eliminarEntrega
IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'eliminarEntrega' AND type = 'P')
DROP PROCEDURE eliminarEntrega 
GO
CREATE PROCEDURE eliminarEntrega
@uClave NUMERIC(5,0),
@uRFC CHAR(13),
@uNumero NUMERIC(5,0),
@uFecha datetime,
@uCantidad NUMERIC(8,2)
AS
	DELETE FROM Entregan WHERE  Clave=@uClave AND RFC=@uRFC AND Numero=@uNumero AND Fecha=@uFecha AND Cantidad=@uCantidad

GO
---PROBAR 
IF EXISTS (SELECT name FROM sysobjects
WHERE name = 'queryMaterial' AND type = 'P')
DROP PROCEDURE queryMaterial
GO

CREATE PROCEDURE queryMaterial
    @udescripcion VARCHAR(50),
    @ucosto NUMERIC(8,2)

AS
    SELECT * FROM Materiales WHERE descripcion
    LIKE '%'+@udescripcion+'%' AND costo > @ucosto
GO
--
EXECUTE queryMaterial 'Lad',20 
execute crearProyecto 01, 'CIT IBIZA'
execute crearProveedores 'BIMB120125489','BIMBO'

SELECT * FROM Proveedores
SELECT * FROM Proyectos
--
