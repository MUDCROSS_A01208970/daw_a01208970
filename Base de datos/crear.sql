CREATE TABLE Proyectos(
	Numero numeric(10),
	Denominacion varchar(50)
)
CREATE TABLE Proveedores(
	RFC varchar(15),
	RazonSocial char(50)
)
CREATE TABLE Materiales(
	Clave numeric(5),
	Descripcion varchar(50),
	Costo numeric(8,2)
)
CREATE TABLE Entregan(
	Clave numeric(5),
	RFC char(13),
	Numero numeric(5),
	Fecha datetime,
	Cantidad numeric(8,2)
)