set dateformat dmy
select SUM(e.Cantidad) as 'Total ', sum(e.Cantidad*((m.Costo*((m.PorcentajeImpuesto/100)+1)))) as 'Importe'
from entregan e, Materiales m
where m.Clave=e.Clave and (e.Fecha BETWEEN '01-01-1997' and '31-12-1997')

-- 2
select distinct pr.RazonSocial, Count(e.Numero) as 'Numero de entregas',sum(e.Cantidad*((m.Costo*((m.PorcentajeImpuesto/100)+1)))) as 'Importe'
from Proveedores pr, Entregan e, Materiales m
where pr.RFC=e.RFC and m.Clave=e.Clave
group by  pr.RazonSocial

--3
select distinct m.Clave,m.Descripcion, sum(e.Cantidad) as 'Cantidad de entregas',min(e.Cantidad) as 'Minimo',max(e.cantidad) as 'Maximo',sum(e.Cantidad*((m.Costo*((m.PorcentajeImpuesto/100)+1)))) as 'Importe'
from materiales m, Entregan e
where m.Clave=e.Clave
group by m.Clave,m.Descripcion
having avg(e.Cantidad) > 400

--4
select  pr.RazonSocial, avg(e.Cantidad) as 'Promedio de material', m.Clave,m.Descripcion
from Proveedores pr, Entregan e, Materiales m
where m.Clave=e.Clave and pr.RFC=e.RFC
group by pr.RazonSocial, m.Clave,m.Descripcion
having avg(e.Cantidad)<500

--5
select  pr.RazonSocial, avg(e.Cantidad) as 'Promedio de material', m.Clave,m.Descripcion
from Proveedores pr, Entregan e, Materiales m
where m.Clave=e.Clave and pr.RFC=e.RFC
group by pr.RazonSocial, m.Clave,m.Descripcion
having avg(e.Cantidad)<370  or avg(e.Cantidad)>450

--Insercion 
INSERT INTO Materiales values (1440,'Tablaroca',150.00,2.05)
INSERT INTO Materiales values (1450,'Pegamento 5000',80.00,2.06)
INSERT INTO Materiales values (1460,'Pegamento 350',50.00,2.07)
INSERT INTO Materiales values (1470,'Madera',55.00,2.39)
INSERT INTO Materiales values (1480,'Plastilina industrial',35.00,2.39)

--6
select m.Clave,m.Descripcion
from Materiales m
where m.Clave
NOT IN(
select clave 
from Entregan
)

--7
select  pr.RazonSocial
from Proveedores pr, Entregan e, Proyectos p
where pr.RFC=e.RFC and p.Numero=e.Numero and p.Denominacion like 'Vamos%' and p.Denominacion
IN(
select p.Denominacion
from Proyectos p, Entregan e
where p.Numero=e.Numero and p.Denominacion like 'Quer%'
)

--8

select  m.Descripcion
from Materiales m 
where   m.Clave 
NOT IN (
select e.Clave
from Entregan e, Proyectos p
where e.Numero=p.Numero  and p.Denominacion like 'CIT Yucatan'
)
select * from Proyectos p, entregan e where e.Numero=p.Numero

--9

select pr.RazonSocial, avg(e.Cantidad) as 'Cantidad entregada'
from Proveedores pr, Proyectos p, Entregan e 
where pr.RFC=e.RFC and p.Numero=e.Numero 
group by pr.RazonSocial
having avg(e.Cantidad)> (
select avg(e.Cantidad)
from  Entregan e
where  e.RFC like 'VAGO780901' )

--10

select pr.RFC,pr.RazonSocial
from Proveedores pr, Proyectos p, Entregan e 
where pr.RFC=e.RFC and p.Numero=e.Numero and pr.RazonSocial like 'Info%'
group by  pr.RFC,pr.RazonSocial
having (
select sum(e.Cantidad)
from Proveedores pr, Proyectos p, Entregan e
where pr.RFC=e.RFC and p.Numero=e.Numero and e.Fecha between 01-01-2000 and 31-12-2000 )
> (
select sum(e.Cantidad)
from Proveedores pr, Proyectos p, Entregan e
where pr.RFC=e.RFC and p.Numero=e.Numero and e.Fecha between 01-01-2001 and 31-12-2001 )
