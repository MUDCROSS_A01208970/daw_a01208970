<?php include("header.html"); ?>
<div class="container">
	<div class="row">
		<H1 class="center">Laboratorio 9</H1>
		<div class="col s12 m8 l6">
			<section id="preguntas">
				<H2 class="center">Preguntas</H2>         
				<article>
					<h6>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.</h6>
					<p>Muestra el estado actual de PHP. Incluye información sobre la versión de PHP, información del servidor etc. </p>
					<ol>
						<li>phpinfo(1): La línea de configuración, ubicación de php.ini, fecha de compilación, servidor Web, sistema y más.</li>
						<li>phpinfo(64): Información de Licencia de PHP. </li>
						<li>phpinfo(32): Muestra todas las variables predefinidas de EGPCS (Environment, GET, POST, Cookie, Server).</li>
					</ol>
					<h6>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</h6>
					<p>Hay que activar el archivo php.ini-production, esto nos configurará el entorno de producción para PHP. Contiene ajustes que mantienen la seguridad, el rendimiento y las mejores prácticas activamente. </p>
					<h6>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.</h6>
					<p>Porque se ejecutan archivos PHP para establecer la comunicación entre cliente-servidor, por ejemplo: Cuando pinchamos sobre un enlace hipertexto, en realidad lo que pasa es que establecemos una petición de un archivo HTML residente en el servidor (un ordenador que se encuentra continuamente conectado a la red) el cual es enviado e interpretado por nuestro navegador (el cliente).</p>
				</article>
			</section>
		</div>
        
        
        <p> Ejercicio 1 (Arreglos)</p>
        <form action= "arreglos.php" method="GET">
				<p class="center">Introduce tu Arreglo de 4 localidades</p>
				<label for="num1">Número 1</label>
				<input type="number" name="num1" id="num1" value=0>
				<br>
				<label for="num2">Número 2</label>
				<input type="number" name="num2" id="num2" value=0>
				<br>
				<label for="num3">Número 3</label>
				<input type="number" name="num3" id="num3" value=0>
				<br>
				<label for="num4">Número 4</label>
				<input type="number" name="num4" id="num4" value=0>
				<br>
				<label for="problem">Elige el ejercicio a resolver</label>
				<select name="problem" class="browser-default">
					<option value="ex1">Ejercicio 1 (Promedio)</option>
					<option value="ex2">Ejercicio 2 (Mediana)</option>
					<option value="ex3">Ejercicio 3 (Todo)</option>
				</select>
				<br>
				<input type="submit" name="submit">
			</form>
        <p class="center">Ejercicio 4</p>
			<form action="potencias.php" method="GET">
				<p class="center">Cuadrados y Cubos</p>
				<label for="pow">Escribe un numero </label>
				<input type="number" value=0 name="pow" id="pow">
				<br>
				<input type="submit" name="submit">
			</form>
			<h6 class="center">Ejercicio 5</h6>
			<form action="nuevo.php" method="GET">
				<p class="center">IMC</p>
				<label for="peso">Peso</label>
				<input type="number" value=0 name="peso" id="peso">
				<br>
				<input type="submit" name="submit">
			</form>
		
		</div>
	</div>

<?php include("footer.html"); ?>
        
        
        
        
        