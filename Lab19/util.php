<?php 
    function conectDb(){
        $servername="localhost";
        $username= "root";
        $password= "";
        $dbname="Servicios";
        
        $con = mysqli_connect($servername,$username,$password,$dbname);
        
        if(!$con){
            die("Connection failed: " . mysqli_connect_error());
        }
        return $con;
    
    }
    function closeDb($mysql){
        mysqli_close($mysql);
}

function getservicios(){
    $con= conectDb();
    $sql ="SELECT IDServicios,Nombre,Descripcion,IDDepartamento FROM Servicios";
    $result = mysqli_query($con,$sql);
    closeDb($con);
      return $result;
}

function getServicioByDepartamento($Depa){
    
    $conn=conectDb();
    
    $sql= "SELECT IDServicios,Nombre,Descripcion,IDDepartamento FROM Servicios WHERE IDDepartamento = '".$Depa."'";
    $result= mysqli_query($conn,$sql);
      if(mysqli_num_rows($result)>0){
    echo '<table><thead><h2>Listado de todas los servicios</h2><tr><th>Nombre
    </th><th>Descripcion</th><th>IDDepartamento</th></tr></thead><tbody>';
    //Imprimir cada fila
    while($row=mysqli_fetch_assoc($result)){
      echo '<tr>';
      echo '<td>'.$row["Nombre"].'</td>';
      echo '<td>'.$row["Descripcion"].'</td>';
      echo '<td>'.$row["IDDepartamento"].'</td>';
      echo '</tr>';
    }
  }
    echo '</tbody></table>';
    
    mysqli_free_result($result);
    closeDb($conn);
    return $result;
}


function getServicioByNombre($name){
    
    $conn=conectDb();
    
    $sql= "SELECT IDServicios,Nombre,Descripcion,IDDepartamento FROM Servicios WHERE Nombre LIKE  '%".$name."%'";
    $result= mysqli_query($conn,$sql);
      if(mysqli_num_rows($result)>0){
    echo '<table><thead><h2>Listado de todas los servicioss</h2><tr><th>Nombre
    </th><th>Descripcion</th><th>IDDepartamento</th></tr></thead><tbody>';
    //Imprimir cada fila
    while($row=mysqli_fetch_assoc($result)){
      echo '<tr>';
      echo '<td>'.$row["Nombre"].'</td>';
      echo '<td>'.$row["Descripcion"].'</td>';
      echo '<td>'.$row["IDDepartamento"].'</td>';
      echo '</tr>';
    }
  }
    echo '</tbody></table>';
    
    mysqli_free_result($result);
    closeDb($conn);
    return $result;
}
function insertnew($Nombre,$Descripcion,$IDDepartamento){
    $conn=conectDb();
    $sql ="INSERT into Servicios (Nombre,Descripcion,IDDepartamento)   VALUES (?,?,?); ";
     if (!($statement = $conn->prepare($sql))) {
            die("Preparation failed: (" . $conn->errno . ") " . $conn->error);
        }
    
   $Nombre = $conn->real_escape_string($Nombre);
     $Descripcion = $conn->real_escape_string($Descripcion);
   $IDDepartamento = $conn->real_escape_string($IDDepartamento); 

   if (!$statement->bind_param("sss", $Nombre, $Descripcion, $IDDepartamento)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
         // Executing the statement
         if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
          } 
 
    closeDb($conn);
}

function delete_by_ID($ID){
    $conn=conectDb();
    $sql="DELETE FROM Servicios WHERE IDServicios = '".$ID."' ";
       $ID = $conn->real_escape_string($ID);
        
    $result= mysqli_query($conn,$sql);
    closeDb($conn);
    return $result;
}
function update_Servicio($IDServicios,$Nombre,$Descripcion,$IDDepartamento){
   $conn=conectDb();
    $sql ="UPDATE Servicios SET Nombre='$Nombre', Descripcion='$Descripcion', IDDepartamento='$IDDepartamento' WHERE IDServicios = '".$IDServicios."' ";
    $result = mysqli_query($conn,$sql);
     
    $id = $conn->real_escape_string($IDServicios);
   $Nombre = $conn->real_escape_string($Nombre);
     $Descripcion = $conn->real_escape_string($Descripcion);
   $IDDepartamento = $conn->real_escape_string($IDDepartamento); 
    closeDb($conn);
    return $result;
    
}
?>