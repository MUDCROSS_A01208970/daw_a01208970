CREATE TABLE Clientes_Banca(
 NoCuenta varchar (5) NOT NULL PRIMARY KEY,
 Nombre varchar (30),
 Saldo numeric(10,2)
)

CREATE TABLE Tipos_Movimiento(
 ClaveM varchar (2) NOT NULL PRIMARY KEY,
 Descripcion varchar (30)
)
CREATE TABLE Realizan(
 Fecha datetime,
 Monto numeric (10,2),
 NoCuenta varchar (5) NOT NULL,
 ClaveM varchar (2) NOT NULL ,
)
ALTER TABLE Realizan
ADD PRIMARY KEY (NoCuenta, ClaveM);


-- Transacciones


BEGIN TRANSACTION PRUEBA1 
INSERT INTO CLIENTES_BANCA VALUES('001', 'Manuel Rios Maldonado', 9000); 
INSERT INTO CLIENTES_BANCA VALUES('002', 'Pablo Perez Ortiz', 5000); 
INSERT INTO CLIENTES_BANCA VALUES('003', 'Luis Flores Alvarado', 8000); 
COMMIT TRANSACTION PRUEBA1

SELECT * FROM CLIENTES_BANCA 

BEGIN TRANSACTION PRUEBA2 
INSERT INTO CLIENTES_BANCA VALUES('004','Ricardo Rios Maldonado',19000)
INSERT INTO CLIENTES_BANCA VALUES('005','Pablo Ortiz Arana',15000)
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Manuel Alvarado',18000) 

SELECT * FROM CLIENTES_BANCA 

--

ROLLBACK TRANSACTION PRUEBA2 

--Atomicidad
BEGIN TRANSACTION PRUEBA3 
INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico'); 
INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla'); 
COMMIT TRANSACTION PRUEBA3 

BEGIN TRANSACTION PRUEBA4 
INSERT INTO Realizan VALUES(GETDATE(),500,'001','A'); 
UPDATE CLIENTES_BANCA SET SALDO = SALDO -500 
WHERE NoCuenta='001' 
COMMIT TRANSACTION PRUEBA4 


select * from Tipos_Movimiento
select * from Realizan

--manejando fallas

BEGIN TRANSACTION PRUEBA5 
INSERT INTO CLIENTES_BANCA VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Camino Ortiz',5000); 
INSERT INTO CLIENTES_BANCA VALUES('001','Oscar Perez Alvarado',8000); 


IF @@ERROR = 0 
COMMIT TRANSACTION PRUEBA5 
ELSE 
BEGIN 
PRINT 'A transaction needs to be rolled back' 
ROLLBACK TRANSACTION PRUEBA5 
END 
 
 select * from Clientes_Banca

 --Procedures
 IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'REGISTRAR_RETIRO_CAJERO ' AND type = 'P')
DROP PROCEDURE REGISTRAR_RETIRO_CAJERO  
GO
CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO 
@uNoCuenta varchar(5),
@umonto numeric(10,2)
AS

BEGIN TRANSACTION PRUEBA10
	UPDATE Realizan SET NoCuenta=@uNoCuenta,Monto=Monto-@umonto WHERE  NoCuenta=@uNoCuenta
	
IF @@ERROR = 0 
COMMIT TRANSACTION PRUEBA10 
ELSE 
BEGIN 
PRINT 'A transaction needs to be rolled back' 
ROLLBACK TRANSACTION PRUEBA10 
END 
 
GO
 --Procedures 2
 IF EXISTS( SELECT name FROM sysobjects	WHERE  name= 'REGISTRAR_DEPOSITO_VENTANILLA  ' AND type = 'P')
DROP PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA   
GO
CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA  
@uNoCuenta varchar(5),
@umonto numeric(10,2)
AS
BEGIN TRANSACTION PRUEBA11
	UPDATE Realizan SET NoCuenta=@uNoCuenta,Monto=Monto+@umonto WHERE  NoCuenta=@uNoCuenta

IF @@ERROR = 0 
COMMIT TRANSACTION PRUEBA11 
ELSE 
BEGIN 
PRINT 'A transaction needs to be rolled back' 
ROLLBACK TRANSACTION PRUEBA11 
END 
 
GO

execute REGISTRAR_DEPOSITO_VENTANILLA '001',500
select * from Realizan

